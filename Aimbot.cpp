#include "SDK.h"

CAimbot gAimbot;

int CAimbot::GetHitbox(const char* szModelname)
{
	if(szModelname[14] == 's' && szModelname[15] == 'c'){ if(cvars.Spot == 1) return 6; else return 3;}
	else if(szModelname[14] == 's' && szModelname[15] == 'o'){ if(cvars.Spot == 1) return 6; else return 3;}
	else if(szModelname[14] == 's' && szModelname[15] == 'n'){ if(cvars.Spot == 1) return 6; else return 13;}
	else if(szModelname[14] == 's' && szModelname[15] == 'p'){ if(cvars.Spot == 1) return 6; else return 3;}
	else if(szModelname[14] == 'p'){ if(cvars.Spot == 1) return 6; else return 6;}
	else if(szModelname[14] == 'e'){ if(cvars.Spot == 1) return 8; else return 5;}
	else if(szModelname[14] == 'm'){ if(cvars.Spot == 1) return 9; else return 6;}
	else if(szModelname[14] == 'h'){ if(cvars.Spot == 1) return 11; else return 12;}
	else if(szModelname[14] == 'd'){ if(cvars.Spot == 1) return 16; else return 5;}
	else return 0;
}

void CAimbot::Init()
{
	this->fBuf = 0;
	this->fNearestDist = 0;
	this->iNearestEnt = 0;
}

void CAimbot::GetNearest(int i, CBaseEntity* pLocal, CBaseEntity* pEntity, player_info_t &pInfo)
{
	int* teamnum = (int*)((DWORD)pEntity+0x94);
	int* teamnum2 = (int*)((DWORD)pLocal+0x94);
	int* health = (int*)((DWORD)pEntity+0x8C);
	int* lifestate = (int*)((DWORD)pEntity + 0x45A);

	if(*teamnum != *teamnum2 && *lifestate)
	{
		this->fBuf = 0;
		
		if(cvars.Mode == 2)
			this->fBuf = GetDistance(pLocal, pEntity) / 22.0f;
		else if(cvars.Mode == 3)
			this->fBuf = *health;

		
		this->iHitbox = this->GetHitbox(hl2.pModelInfo->GetModelName(pEntity->GetModel()));
		if(!GetBonePosition(this->iHitbox, this->vPlayer[i], this->qPlayer, i))
			return;

		if((this->fBuf < this->fNearestDist || !this->fNearestDist) && (!cvars.Aimtrough && (GetVisible(i, gEsp.vEyeHeight, this->vPlayer[i]) || ((GetDistance( pLocal, pEntity)/22.0f) < 9)) || cvars.Aimtrough))
		{
			this->fNearestDist = this->fBuf;
			this->iNearestEnt = i;
		}
	}
}
extern int centerx, centery;
void CAimbot::Do(int i, CBaseEntity* pLocal, CBaseEntity* pEntity, player_info_t &pInfo)
{
	int* teamnum = (int*)((DWORD)pEntity+0x94);
	int* teamnum2 = (int*)((DWORD)pLocal+0x94);
	int* health = (int*)((DWORD)pEntity+0x8C);
	int* lifestate = (int*)((DWORD)pEntity + 0x45A);
	int* lifestate2 = (int*)((DWORD)pLocal + 0x45A);

	if(!cvars.Aimbot)
		return;

	if(cvars.Mode > 1)
		return;

	if(cvars.Aimbot && *lifestate && *health > 1)
	{
		if(!WorldToScreen(this->vPlayer[i], this->vPosition))
			return;
		
		if((cvars.Aimtrough == 0 && *teamnum != *teamnum2 && (GetVisible(i, gEsp.vEyeHeight , this->vPlayer[i]) || (GetDistance(pLocal, pEntity)/22.0f) < 9 )) || (cvars.Aimtrough == 1 && *teamnum != *teamnum2))
		{
			if(IsInPixelFov(vPosition.x, vPosition.y, cvars.Fov))
			{
				if(cvars.Draw)
					DrawQuad(centerx-8, centery-8, 16, 16, 255,255,0,255,1);

				if( (cvars.Aimkey == 0) || (cvars.Aimkey == 1 && GetAsyncKeyState(VK_LBUTTON)) || (cvars.Aimkey == 2 && GetAsyncKeyState(VK_RBUTTON)) || (cvars.Aimkey == 3 && GetAsyncKeyState(VK_MBUTTON)) || (cvars.Aimkey > 4 && GetAsyncKeyState(cvars.Aimkey)) )
				{
					POINT pt = {vPosition.x, vPosition.y};
					ClientToScreen(GetForegroundWindow(), &pt);

					SetCursorPos(pt.x, pt.y);		
				}
			}
		}
	}
}


void CAimbot::DO(CBaseEntity* pLocal)
{	
	if(!cvars.Aimbot)
		return;

	if(cvars.Mode == 1)
		return;

	CBaseEntity *pEntity = pGetBaseEnt(gAimbot.iNearestEnt);
	if(pEntity == NULL)
		return;

	int* teamnum = (int*)((DWORD)pEntity+0x94);
	int* teamnum2 = (int*)((DWORD)pLocal+0x94);
	int* health = (int*)((DWORD)pEntity+0x8C);
	int* lifestate = (int*)((DWORD)pEntity + 0x45A);
	int* lifestate2 = (int*)((DWORD)pLocal + 0x45A);


	if(cvars.Aimbot && *health > 1)
	{
		if(gAimbot.iNearestEnt == 0)
			return;
		
		if(!WorldToScreen(this->vPlayer[gAimbot.iNearestEnt], this->vPosition) /*&& this->vPlayer[gAimbot.iNearestEnt].z >*/ )
			return;

		DrawFilledQuad(vPosition.x, vPosition.y, 3, 3, 255,255,255,255);
		
		if((cvars.Aimtrough == 0 && *teamnum != *teamnum2&& (GetVisible(gAimbot.iNearestEnt, gEsp.vEyeHeight , this->vPlayer[gAimbot.iNearestEnt]) || (GetDistance(pLocal, pEntity)/22.0f) < 9 )) || (cvars.Aimtrough == 1 && *teamnum != *teamnum2))
		{
			if(IsInPixelFov(vPosition.x, vPosition.y, cvars.Fov))
			{
				if(cvars.Draw)
					DrawQuad(centerx-8, centery-8, 16, 16, 255,255,0,255,1);

				if((cvars.Aimkey == 0) || (cvars.Aimkey == 1 && GetAsyncKeyState(VK_LBUTTON)) || (cvars.Aimkey == 2 && GetAsyncKeyState(VK_RBUTTON)) || (cvars.Aimkey == 3 && GetAsyncKeyState(VK_MBUTTON)) || (cvars.Aimkey > 4 && GetAsyncKeyState(cvars.Aimkey)) )
				{
					POINT pt = {vPosition.x, vPosition.y};
					ClientToScreen(GetForegroundWindow(), &pt);

					SetCursorPos(pt.x, pt.y);	

				}
			}
		}
	}
}