#ifndef _DLLMAIN_H_
#define _DLLMAIN_H_

#pragma warning(disable: 4311 4312 4267 4101 4244 4518 4228)

struct CCVARS
{
	int Aimbot;
	int Aimkey;
	int Fov;
	int Spot;
	int Draw;
	int Aimtrough;
	int Visible;
	int Mode;

	int Name;
	int Box;
	int Distance;
	int Health;
	int GUID;
	int Class;
	int Object;

	int Boxsize;
	int Boxa;

	int Crosshair;
	int Autopistol;
	int Radar;
	int Bunnyhop;
	int Speedhack;
	int Speedkey;

	int Menu;

	int Menukey;
	int Panickey;

}; extern CCVARS cvars;

#endif