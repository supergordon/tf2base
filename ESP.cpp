#include "SDK.h"

CESP gEsp;

CESP::CESP()
{
	iHitbox = 0;
	iEspHeight = 8;
}

int range = 2230;
void CESP::DrawRadarPoint(Vector vecOriginx, Vector vecOriginy, QAngle vAngle, int iTeam)
{	
	float dx  = vecOriginx.x - vecOriginy.x;
	float dy  = vecOriginx.y - vecOriginy.y;
	float flAngle = vAngle.y;
	float yaw = (flAngle) * (M_PI/180.0);
	float mainViewAngles_CosYaw = cos(yaw);
	float mainViewAngles_SinYaw = sin(yaw);
	float x =  dy*(-mainViewAngles_CosYaw)  + dx*mainViewAngles_SinYaw;
	float y =  dx*(-mainViewAngles_CosYaw)  - dy*mainViewAngles_SinYaw;
	if(fabs(x)>range || fabs(y)>range)
	{ 
		if(y>x)
		{
			if(y>-x) {
				x = range*x/y;
				y = range;
			}  else  {
				y = -range*y/x; 
				x = -range; 
			}
		} else {
			if(y>-x) {
				y = range*y/x; 
				x = range; 
			}  else  {
				x = -range*x/y;
				y = -range;
			}
		}
	}
	int ScreenX = (68+82) + int( x/range*float(82));
	int ScreenY = (68+82) + int( y/range*float(82));

	int r = 0, b = 0, g = 0;
	if(iTeam == 2)
	{
		r = 255; b = 0; g = 2;
	}
	if(iTeam == 3)
	{
		r = 0; g = 100; b = 255;
	}
	DrawFilledQuad(ScreenX, ScreenY, 3, 3, r, g, b, 255);
}

inline void CESP::SetColors(int iTeamNum)
{
	if(iTeamNum == 2) { this->rgba[0] = 255; this->rgba[1] = 2; this->rgba[2] = 0; this->rgba[3] = 255; }
	else if(iTeamNum == 3) { this->rgba[0] = 0; this->rgba[1] = 100; this->rgba[2] = 255; this->rgba[3] = 255; }
	else if(iTeamNum == 9) { this->rgba[0] = 0; this->rgba[1] = 255; this->rgba[2] = 0; this->rgba[3] = 255; }
}

inline void CESP::SetObjectColors(int iTeamNum)
{
	if(iTeamNum == 2) { this->rgbas[0] = 255; this->rgbas[1] = 2; this->rgbas[2] = 0; this->rgbas[3] = 255; }
	else if(iTeamNum == 3) { this->rgbas[0] = 0; this->rgbas[1] = 100; this->rgbas[2] = 255; this->rgbas[3] = 255; }
	else if(iTeamNum == 9) { this->rgbas[0] = 0; this->rgbas[1] = 255; this->rgbas[2] = 0; this->rgbas[3] = 255; }
}

inline int CESP::Get(int iIndex)
{
	return this->rgba[iIndex];
}

inline int CESP::Gets(int iIndex)
{
	return this->rgbas[iIndex];
}

void CESP::Do(int i, CBaseEntity* pLocal, CBaseEntity* pEntity, player_info_t &pInfo)
{
	if(i == hl2.pEngineFuncs->GetLocalPlayer())
		return;
	
	int* lifestate = (int*)((DWORD)pEntity + 0x45A);
	int* lifestate2 = (int*)((DWORD)pLocal + 0x45A);
	int* teamnum = (int*)((DWORD)pEntity+0x94);
	int* teamnum2 = (int*)((DWORD)pLocal+0x94);
	int* health = (int*)((DWORD)pEntity+0x8C);


	if(*health <= 1 && *lifestate == 0)
		return;
	
	if(cvars.Radar)
		this->DrawRadarPoint(pEntity->GetAbsOrigin(), pLocal->GetAbsOrigin(), pLocal->GetAbsAngles(), *teamnum);

	
	if(!GetBonePosition(this->iHitbox, this->vPlayer, this->qPlayer, i))
		return;

	if(!WorldToScreen(this->vPlayer, this->vScreen))
		return;

	this->SetColors(*teamnum);


	if(cvars.Visible && *teamnum != *teamnum2 && GetVisible(i, gEsp.vEyeHeight, this->vPlayer))
		this->SetColors(9);

	this->iEspHeight = 0;

	if(cvars.Box)
	{
		int radius = 500/(VecDistance(pLocal->GetAbsOrigin(),pEntity->GetAbsOrigin()) / 35);
		DrawQuad(this->vScreen.x-(radius/2), this->vScreen.y-(radius/2), radius, radius, Get(0) ,Get(1) ,Get(2) ,Get(3), cvars.Box);
	}
	if(cvars.Name)
	{
		drawString2(true, this->vScreen.x, this->vScreen.y+this->iEspHeight, Get(0) ,Get(1) ,Get(2) ,Get(3), "-%s-", pInfo.name);
		this->iEspHeight += 8;
	}
	if(cvars.Distance)
	{
		drawString2(true, this->vScreen.x, this->vScreen.y+this->iEspHeight, Get(0) ,Get(1) ,Get(2) ,Get(3), "-%.2f-", GetDistance(pLocal, pEntity)/22.0f);
		this->iEspHeight += 8;
	}
	if(cvars.Health)
	{
		if(cvars.Health == 1)
		{
			drawString2(true, this->vScreen.x, this->vScreen.y+this->iEspHeight, Get(0) ,Get(1) ,Get(2) ,Get(3), "-%d-", *health);
			this->iEspHeight += 8;
		}
		else if(cvars.Health == 2);
	}
	if(cvars.GUID)
	{
		drawString2(true, this->vScreen.x, this->vScreen.y+this->iEspHeight, Get(0) ,Get(1) ,Get(2) ,Get(3), "-%s-", pInfo.guid);
		this->iEspHeight += 8;
	}
	if(cvars.Class)
	{
		char display[50] = "";
		const char* modelname = hl2.pModelInfo->GetModelName(pEntity->GetModel());

		if(modelname[14] == 's' && modelname[15] == 'c') strcpy(display, "Scout");
		else if(modelname[14] == 's' && modelname[15] == 'o') strcpy(display, "Soldier");
		else if(modelname[14] == 'p') strcpy(display, "Pyro");
		else if(modelname[14] == 'd') strcpy(display, "Demoman");
		else if(modelname[14] == 'h') strcpy(display, "Heavy");
		else if(modelname[14] == 'e') strcpy(display, "Engineer");
		else if(modelname[14] == 'm') strcpy(display, "Medic");
		else if(modelname[14] == 's' && modelname[15] == 'n') strcpy(display, "Sniper");
		else if(modelname[14] == 's' && modelname[15] == 'p') strcpy(display, "Spy");

		drawString2(true, this->vScreen.x, this->vScreen.y+this->iEspHeight, Get(0) ,Get(1) ,Get(2) ,Get(3), "-%s-", display);
		this->iEspHeight += 8;
	}
}

void CESP::DoObject(CBaseEntity* pLocal, CBaseEntity* pEntity)
{

	int* teamnum = (int*)((DWORD)pEntity+0x94);
	this->SetObjectColors(*teamnum);

	if(WorldToScreen(pEntity->GetAbsOrigin(), this->vObject) == false)
		return;

	const char* szClassName = pEntity->GetClientClass()->GetName();


	if(strstr(szClassName, "CObjectTeleporter"))
		drawString2(true, vObject.x, vObject.y, Gets(0) ,Gets(1) ,Gets(2) ,255, "Teleporter");

	if(strstr(szClassName, "CObjectSentrygun" ))
		drawString2(true, vObject.x, vObject.y, Gets(0) ,Gets(1) ,Gets(2) ,255, "Sentry");

	if(strstr(szClassName, "CObjectDispenser" ))
		drawString2(true, vObject.x, vObject.y, Gets(0) ,Gets(1) ,Gets(2) ,255, "Dispenser");
}