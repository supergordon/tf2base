#include "SDK.h"


CCVARS cvars;
HL2 hl2;

CClient gClient;
CClient* pClient = new CClient;

Scene_t pEndScene = NULL;
Reset_t pReset = NULL;

LPDIRECT3DDEVICE9 pD3Ddev;

bool bDone = false;
bool Panickey = 0;

//#include "NoSpread.h"
float lali = 0;

//cNoSpread gNospread;
C_BaseCombatWeapon* pWeapon = 0;
void __stdcall hkCreateMove (int sequence_number, float input_sample_frametime, bool active)
{
	gClient.CreateMove( sequence_number, input_sample_frametime, active );

	if(hl2.pInput && !Panickey)
	{
		if (!hl2.pEngineFuncs->IsConnected()
			 || !hl2.pEngineFuncs->IsInGame())
		return;

		
		CUserCmd* cmd = hl2.pInput->GetUserCmd( sequence_number );
		CBaseEntity* pLocal = pGetBaseEnt(hl2.pEngineFuncs->GetLocalPlayer());


		try
		{
		
			if(cvars.Bunnyhop)
			{
				const char* modelname = hl2.pModelInfo->GetModelName(pLocal->GetModel());
				if(modelname[14] != 's' && modelname[15] != 'c')
				{
					int flags = *(int*)((DWORD)pLocal + 0x2DC);
					if (cmd->buttons & IN_JUMP)
					{
						if(!(flags &FL_ONGROUND) )
							cmd->buttons &= ~IN_JUMP;
					}
				}			
			}
		
		}
		catch(...)
		{
			return;
		}
		
		if(cvars.Autopistol)
		{
			//pWeapon = 0;
			pWeapon = GetActiveWeapon();
			if(pWeapon == NULL)
				return;

			lali = *(float*)((DWORD)pWeapon + 0x80C);

			if(strstr(hl2.pModelInfo->GetModelName(pWeapon->GetModel()), "pistol") && cmd->buttons & IN_ATTACK)
			{
				static bool bAttack = false;

				if (bAttack) cmd->buttons |=  IN_ATTACK;
				else cmd->buttons &= ~IN_ATTACK;

				bAttack = !bAttack;
			}
		}

	}
}

HMODULE hClient = NULL, hEngine = NULL;
DWORD dwGetActiveWeapon = NULL;
void InitializeOffsets()
{
	Sleep(2000);

	CreateInterfaceFn Client = NULL, Engine = NULL;
	
	
	while(!hClient)
	{
		hClient = GetModuleHandle("client.dll");
		Sleep(50);
	}

	hEngine = GetModuleHandle("engine.dll");

	Client = (CreateInterfaceFn)GetProcAddress(hClient, "CreateInterface");
	Engine = (CreateInterfaceFn)GetProcAddress(hEngine, "CreateInterface");
	
	//18 createmove
	hl2.pClientFuncs = (IBaseClientDLL*)Client(CLIENT_DLL_INTERFACE_VERSION, 0);
	hl2.pEngineFuncs = (IVEngineClient*)Engine("VEngineClient013", 0); //fix
	hl2.pEnginetrace = (IEngineTrace*)Engine(INTERFACEVERSION_ENGINETRACE_CLIENT, 0);
	hl2.pEntityList	 = (IClientEntityList*)Client(VCLIENTENTITYLIST_INTERFACE_VERSION, 0); //has not changed
	hl2.pEnginevgui	 = (IEngineVGui*)Engine("VEngineVGui001", 0);
	hl2.pModelInfo	 = (IVModelInfoClient*)Engine(VMODELINFO_CLIENT_INTERFACE_VERSION, NULL );

	hl2.pModelInfo	 = (IVModelInfoClient*)Engine(VMODELINFO_CLIENT_INTERFACE_VERSION, 0);
	hl2.pDebugOverlay= (IVDebugOverlay*)Engine(VPHYSICS_DEBUG_OVERLAY_INTERFACE_VERSION, 0);
	//client.dll + 0x79170
	//+ 0x74CC8
	
	/*dwGetActiveWeapon = (DWORD)dwFindPattern((DWORD)hClient, 0x0058C000, (BYTE*)"\xE8\x00\x00\x00\x00\x85\xC0\x75\x00\xC3\x8B\x10\x8B\xC8\x8B\x82\x00\x00\x00\x00\xFF\xE0", "x????xxx?xxxxxxx????xx");
	LogText("%X", dwGetActiveWeapon);*/
	if( hl2.pClientFuncs )
	{
		DWORD dwSave[4]= {0};

		DWORD** dwClient = (DWORD**)Client( CLIENT_DLL_INTERFACE_VERSION , NULL );
		if ( dwClient )
		{
			memcpy( (void*) &gClient,(void*)*dwClient , sizeof (CClient) );
			pClient = (CClient*)*dwClient;

			if( pClient )
			{	
				if( VirtualProtect( (LPVOID)&pClient->CreateMove, 4, PAGE_EXECUTE_READWRITE, &dwSave[0] ) )
					pClient->CreateMove = &hkCreateMove;

				if ( !hl2.pInput )
				{
					hl2.pInput = (CInput*)*(PDWORD)*(PDWORD)((DWORD)gClient.CreateMove + 0x28);
				}
			}
		}
	}

	bDone = true;
}


int real_x = 0, real_y = 0;
int centerx = 0, centery = 0;
int i = 0;

HRESULT APIENTRY nEndScene(LPDIRECT3DDEVICE9 pDevice)
{
	_asm pushad;

	static bool bInit = false;
	if(!bInit)
	{
		if(hl2.pEngineFuncs)
		{
			hl2.pEngineFuncs->ExecuteClientCmd("clear; echo TF2 Multihack by Gordon`");
			//MessageBoxA(0,0,0,0);
			real_x = GetSystemMetrics(SM_CXSCREEN); real_x /= 2;
			real_y = GetSystemMetrics(SM_CYSCREEN); real_y /= 2;
			//hl2.pEngineFuncs->GetScreenSize(centerx, centery); centerx /= 2; centery /= 2;
			bInit = true;
		}
	}

	pD3Ddev = pDevice;

	if(GetAsyncKeyState(cvars.Menukey) & 1)
	{
		if(cvars.Menu) cvars.Menu = false;
		else if(!cvars.Menu) cvars.Menu = true;
	}
	
	
	if (!hl2.pEngineFuncs->IsConnected()
	|| !hl2.pEngineFuncs->IsInGame()
	||  hl2.pEngineFuncs->Con_IsVisible()
	//|| !hl2.pEngineFuncs->IsDrawingLoadingImage()
	|| hl2.pEngineFuncs->IsLevelMainMenuBackground())
		goto retn;
	
	if(GetAsyncKeyState(cvars.Panickey) & 1)
	{
		if(Panickey) Panickey = false;
		else if(!Panickey) Panickey = true;
	}
	

	if(Panickey) goto retn;

	Crosshair(cvars.Crosshair);


	if(cvars.Radar)
	{
		DrawFilledQuad(68,68,82*2,82*2,0,0,0,255);
		DrawQuad(68,68,82*2,82*2,255,255,255,255,2);
		DrawFilledQuad(68, 68+(82), 82*2, 1, 0,255,0,255);
		DrawFilledQuad(68+(82), 68, 1, 82*2, 0,255,0,255);
	}

	if(cvars.Menu)
		gMenu.Draw();

	/*C_BaseCombatWeapon* pWeapon = 0;
	pWeapon = GetActiveWeapon();
	if(pWeapon == NULL)
		goto retn;*/

	

	gAimbot.Init();

	CBaseEntity* pLocal = pGetBaseEnt(hl2.pEngineFuncs->GetLocalPlayer());
	if(pLocal == NULL)
		goto retn;

	//drawString2(0, 10, 10, 255, 255, 255, 255, "%X", pLocal);
	if(GetAsyncKeyState(VK_NUMPAD5)) {
	char keke[200] = "";
	sprintf(keke, "echo %X", pLocal);
	hl2.pEngineFuncs->ExecuteClientCmd(keke);
	}
	//LogText("%X", pLocal);

	gEsp.vEyeHeight = pLocal->GetAbsOrigin();
	gEsp.vEyeHeight.z += 70;

	for(int i = 0; i < hl2.pEntityList->GetMaxEntities(); i++)
	{
		CBaseEntity* pEntity = pGetBaseEnt(i);
		if(pEntity == NULL)
			goto da;

		player_info_t pInfo;
		if(hl2.pEngineFuncs->GetPlayerInfo(i, &pInfo))
		{
			gEsp.Do(i, pLocal, pEntity, pInfo);
			gAimbot.GetNearest(i, pLocal, pEntity, pInfo);
			gAimbot.Do(i, pLocal, pEntity, pInfo);

		}
		else
			gEsp.DoObject(pLocal, pEntity);

		da:;
	}

	gAimbot.DO(pLocal);
//sv_cheats 1;bot;bot;bot;bot -team red;bot -team red;bot -team red;



	retn:;
	_asm popad;
	return pEndScene(pDevice);
}