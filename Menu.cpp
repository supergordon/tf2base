#include "SDK.h"

//
extern void drawString2(bool bCenter, int x, int y, int r, int g, int b, int a, const char *pInput, ...);

cMenu gMenu;
//
cMenu::cMenu()
{
	xx = 100;
	yy = 100;
	Abstand = 0;
	Elemente = 0;
	iSelect = 1;
}
//
struct sExpend
{
	int Element;
	char Text[255];
}; sExpend pExpend[40];
//
//
void cMenu::DrawEntry(char* Text, int Cvar,int r, int g, int b, int w)
{
	drawString2(false,xx+w-5,yy+Abstand,r,g,b,255,"%s",Text);
	if(w != 0)
	{
		char buf[10] = ""; sprintf(buf, "%d", Cvar);
		drawString2(false,xx+w+75,yy+Abstand,r,g,b,255,"%d",Cvar);
	}
		
	Abstand += 12;
	pExpend[Elemente].Element = Elemente;
	strcpy(pExpend[Elemente].Text,Text);
	++Elemente;
}
//
void cMenu::Draw()
{

	Abstand = 0;
	
	DrawFilledQuad(xx-20,yy-25,150,(Elemente*12)+20,0,0,0,255);
	DrawQuad(xx-20,yy-25,150,(Elemente*12)+20,255,255,255,255,2);
	DrawFilledQuad(xx-20,yy-10,150,1,255,255,255,255);
	drawString2(true,xx-20+75,yy-20,0,255,0,255,"Gordon` TF2");
	//font->Print(xx-20+75, yy-23, 0xFF00FF00, "Gordon` BF2", FT_CENTER);
	Elemente = 1;

	DrawEntry("Aimbot",0,0,255,0,0);
	if(Aimbot)
	{
		DrawEntry("Bot",cvars.Aimbot,255,255,255,20);
		DrawEntry("Key",cvars.Aimkey,255,255,255,20);
		DrawEntry("Fov",cvars.Fov,255,255,255,20);
		DrawEntry("Spot",cvars.Spot,255,255,255,20);
		DrawEntry("Draw",cvars.Draw,255,255,255,20);
		DrawEntry("Mode",cvars.Mode,255,255,255,20);
		DrawEntry("Aimtrough",cvars.Aimtrough,255,255,255,20);
	}

	DrawEntry("ESP",0,0,255,0,0);
	if(ESP)
	{
		DrawEntry("Name",cvars.Name,255,255,255,20);
		DrawEntry("Box",cvars.Box,255,255,255,20);
		DrawEntry("Distance",cvars.Distance,255,255,255,20);
		DrawEntry("Health",cvars.Health,255,255,255,20);
		DrawEntry("SteamID",cvars.GUID,255,255,255,20);
		DrawEntry("Class",cvars.Class,255,255,255,20);
		DrawEntry("Object",cvars.Object,255,255,255,20);
		DrawEntry("Visible",cvars.Visible,255,255,255,20);
	}

	DrawEntry("Visual",0,0,255,0,0);
	if(Visual)
	{
		DrawEntry("Crosshair",cvars.Crosshair,255,255,255,20);
		DrawEntry("Autopistol",cvars.Autopistol,255,255,255,20);
		DrawEntry("Bunnyhop",cvars.Bunnyhop,255,255,255,20);
		DrawEntry("Radar",cvars.Radar,255,255,255,20);
		DrawEntry("Speedhack",cvars.Speedhack,255,255,255,20);
	}


	Control();

	DrawFilledQuad(xx-13,yy-10+(iSelect*12),3,3,0,255,0,255);
}
//
///////////////////////////////////////////////////////////////////////////////////////////////////
//
void cMenu::Control()
{
	if( GetKeyState( VK_UP ) < 0 && iPress != VK_UP )
	{
		iSelect--;
		if( iSelect <= 0 ) 
		{
			iSelect = (Elemente-1);
		}
		iPress = VK_UP;
	}
	else if( GetKeyState( VK_DOWN ) < 0 && iPress != VK_DOWN )
	{
		iSelect++;
		if( iSelect > (Elemente-1) )
		{
			iSelect = 1;				
		}
		iPress = VK_DOWN;
	}
	else if( GetKeyState( VK_LEFT ) < 0 && iPress != VK_LEFT )
	{
		for(int i = 0; i < 40; i++)
		{
			if(iSelect == pExpend[i].Element)
			{
				Ueberschriften(i,false);
				Aimbots(i, false);
				Esp(i, false);
				Visuals(i, false);
			//	Others(i, false);
			}
		}
		iPress = VK_LEFT;
	}
	else if( GetKeyState( VK_RIGHT ) < 0 && iPress != VK_RIGHT )
	{
		for(int i = 0; i < 40; i++)
		{
			if(iSelect == pExpend[i].Element)
			{
				Ueberschriften(i,true);
				Aimbots(i, true);
				Esp(i, true);
				Visuals(i, true);
				//Others(i, true);
			}
		}
		iPress = VK_RIGHT;
	}
	else if( !( GetKeyState( VK_UP ) < 0 ) && !( GetKeyState( VK_DOWN ) < 0 ) && !( GetKeyState( VK_LEFT ) < 0 ) && !( GetKeyState( VK_RIGHT ) < 0 ) )
	{
		iPress = 0;
	}
}
//
///////////////////////////////////////////////////////////////////////////////////////////////////
//
void cMenu::Ueberschriften(int i, bool toggle)
{
	if(toggle)
	{
		if(!lstrcmp(pExpend[i].Text,"Aimbot"))
			Aimbot = true;
		if(!lstrcmp(pExpend[i].Text,"ESP"))
			ESP = true;
		if(!lstrcmp(pExpend[i].Text,"Visual"))
			Visual = true;
		if(!lstrcmp(pExpend[i].Text,"Other"))
			Other = true;
	}
	else if(!toggle)
	{
		if(!lstrcmp(pExpend[i].Text,"Aimbot"))
			Aimbot = false;
		if(!lstrcmp(pExpend[i].Text,"ESP"))
			ESP = false;
		if(!lstrcmp(pExpend[i].Text,"Visual"))
			Visual = false;
		if(!lstrcmp(pExpend[i].Text,"Other"))
			Other = false;
	}
}
//
///////////////////////////////////////////////////////////////////////////////////////////////////
//
void cMenu::Aimbots(int i, bool toggle)
{
	if(toggle)
	{
		if(!lstrcmp(pExpend[i].Text,"Bot"))
			cvars.Aimbot = true;
		if(!lstrcmp(pExpend[i].Text,"Key"))
		{
			++cvars.Aimkey;
			if(cvars.Aimkey > 3) cvars.Aimkey = 3;
		}
		if(!lstrcmp(pExpend[i].Text,"Fov"))
		{
			++cvars.Fov;
			if(cvars.Fov > 150) cvars.Fov = 500;
		}
		if(!lstrcmp(pExpend[i].Text,"Spot"))
		{
			++cvars.Spot;
			if(cvars.Spot > 2) cvars.Spot = 2;
		}
		if(!lstrcmp(pExpend[i].Text,"Draw"))
		{
			cvars.Draw = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"Mode"))
		{
			++cvars.Mode;
			if(cvars.Mode > 3) cvars.Mode = 3;
		}
		if(!lstrcmp(pExpend[i].Text,"Aimtrough"))
		{
			cvars.Aimtrough = 1;
		}
	}
	else if(!toggle)
	{
		if(!lstrcmp(pExpend[i].Text,"Bot"))
			cvars.Aimbot = false;
		if(!lstrcmp(pExpend[i].Text,"Key"))
		{
			--cvars.Aimkey;
			if(cvars.Aimkey < 0) cvars.Aimkey = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"Fov"))
		{
			--cvars.Fov;
			if(cvars.Fov < 0) cvars.Fov = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"Draw"))
		{
			cvars.Draw = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"Spot"))
		{
			--cvars.Spot;
			if(cvars.Spot < 1) cvars.Spot = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"Mode"))
		{
			--cvars.Mode;
			if(cvars.Mode < 1) cvars.Mode = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"Aimtrough"))
		{
			cvars.Aimtrough = 0;
		}
	}
}
//
///////////////////////////////////////////////////////////////////////////////////////////////////
//
void cMenu::Esp(int i, bool toggle)
{
	if(toggle)
	{
		if(!lstrcmp(pExpend[i].Text,"Name"))
		{
			cvars.Name = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"Box"))
		{
			++cvars.Box;
			if(cvars.Box > 10) cvars.Box = 10;
		}
		if(!lstrcmp(pExpend[i].Text,"Boxsize"))
		{
			cvars.Boxsize += 10;
			if(cvars.Boxsize > 1000) cvars.Boxsize = 10;
		}
		if(!lstrcmp(pExpend[i].Text,"Distance"))
		{
			cvars.Distance = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"SteamID"))
		{
			cvars.GUID = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"Health"))
		{
			cvars.Health = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"Class"))
		{
			cvars.Class = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"Object"))
		{
			cvars.Object = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"Visible"))
		{
			cvars.Visible = 1;
		}
	}
	else if(!toggle)
	{
		if(!lstrcmp(pExpend[i].Text,"Name"))
		{
			cvars.Name = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"Box"))
		{
			--cvars.Box;
			if(cvars.Box < 0) cvars.Box = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"Boxsize"))
		{
			cvars.Boxsize -= 10;
			if(cvars.Boxsize < 10) cvars.Boxsize = 1000;
		}
		if(!lstrcmp(pExpend[i].Text,"Distance"))
		{
			cvars.Distance = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"SteamID"))
		{
			cvars.GUID = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"Health"))
		{
			cvars.Health = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"Class"))
		{
			cvars.Class = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"Object"))
		{
			cvars.Object = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"Visible"))
		{
			cvars.Visible = 0;
		}
	}
}
//
///////////////////////////////////////////////////////////////////////////////////////////////////
//
void cMenu::Visuals(int i, bool toggle)
{
	if(toggle)
	{
		if(!lstrcmp(pExpend[i].Text,"Crosshair"))
		{
			++cvars.Crosshair;
			if(cvars.Crosshair > 5) cvars.Crosshair = 5;
		}
		if(!lstrcmp(pExpend[i].Text,"Autopistol"))
		{
			cvars.Autopistol = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"Radar"))
		{
			cvars.Radar = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"Bunnyhop"))
		{
			cvars.Bunnyhop = 1;
		}
		if(!lstrcmp(pExpend[i].Text,"Speedhack"))
		{
			++cvars.Speedhack;
			if(cvars.Speedhack > 10) cvars.Speedhack = 10;
		}
	}
	else if(!toggle)
	{
		if(!lstrcmp(pExpend[i].Text,"Crosshair"))
		{
			--cvars.Crosshair;
			if(cvars.Crosshair < 0) cvars.Crosshair = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"Autopistol"))
		{
			cvars.Autopistol = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"Radar"))
		{
			cvars.Radar = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"Bunnyhop"))
		{
			cvars.Bunnyhop = 0;
		}
		if(!lstrcmp(pExpend[i].Text,"Speedhack"))
		{
			--cvars.Speedhack;
			if(cvars.Speedhack < 0) cvars.Speedhack = 0;
		}
	}
}
//
///////////////////////////////////////////////////////////////////////////////////////////////////
//
//void cMenu::Others(int i, bool toggle)
//{
//	if(toggle)
//	{
//		/*if(!lstrcmp(pExpend[i].Text,"cheats"))
//			cvars.sv_cheats = true;
//		if(!lstrcmp(pExpend[i].Text,"consisteny"))
//			cvars.sv_consistency = true;
//		if(!lstrcmp(pExpend[i].Text,"pure"))
//			cvars.sv_pure = true;
//		if(!lstrcmp(pExpend[i].Text,"Speedhack"))
//			cvars.Speedhack = true;*/
//	}
//	else if(!toggle)
//	{
//		/*if(!lstrcmp(pExpend[i].Text,"cheats"))
//			cvars.sv_cheats = false;
//		if(!lstrcmp(pExpend[i].Text,"consisteny"))
//			cvars.sv_consistency = false;
//		if(!lstrcmp(pExpend[i].Text,"pure"))
//			cvars.sv_pure = false;
//		if(!lstrcmp(pExpend[i].Text,"Speedhack"))
//			cvars.Speedhack = false;*/
//	}
//}
//
///////////////////////////////////////////////////////////////////////////////////////////////////