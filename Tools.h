#ifndef _TOOLS_H_
#define _TOOLS_H_

#include "SDK.h"

extern void LogText(char* szText, ...);

extern void *DetourFunc(BYTE *src, const BYTE *dst, const int len);
//extern bool RetourFunc(BYTE *src, BYTE *restore, const int len);

//extern bool BackupMemory(DWORD pvAddress, PBYTE bpBackup, int size);
//extern bool RestoreMemory(DWORD pvAddress, PBYTE bpBackup, int size);

extern DWORD dwFindPattern(DWORD dwAddress,DWORD dwLen,BYTE *bMask,char * szMask);

extern void DrawQuad(int x, int y, int w, int h, int r, int g, int b, int a, int thickness);
extern void DrawFilledQuad(int x, int y, int w, int h, int r, int g, int b, int a);

extern void drawString2(bool bCenter, int x, int y, int r, int g, int b,int a, const char *pInput, ...);
//extern void CalcAngle( const Vector& vSource, const Vector& vDest, QAngle& vAngles );

extern void Crosshair(int type);

extern bool IsInPixelFov (int x, int y, int fov);

extern float VecDistance(Vector from, Vector to);
extern float GetDistance(CBaseEntity* pLocal, CBaseEntity* pEntity);
//extern void VectorSubtract(float *in, float *sub, float *res);

extern CBaseEntity* pGetBaseEnt(IClientEntity* pClientEnt);
extern CBaseEntity* pGetBaseEnt(int iIndex);
extern bool WorldToScreen(const Vector &vOrigin, Vector &vScreen);
extern bool ScreenTransform(const Vector &point, Vector &screen);

extern bool GetBonePosition ( int iBone, Vector& vecOrigin, QAngle qAngles, int index );
extern bool GetVisible(int i, const Vector& vecAbsStart, const Vector& vecAbsEnd) ;
extern C_BaseCombatWeapon* GetActiveWeapon();
#endif