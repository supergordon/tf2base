#include "SDK.h"

void HideModule(HINSTANCE hModule);

char szIniFile[1024] = "";
void GetIniFile( HMODULE hModule ) //neu
{
	GetModuleFileName(hModule, szIniFile, sizeof( szIniFile ));

	for(int i = 0; i<( int )strlen(szIniFile);i++)
	{
		if(szIniFile[strlen(szIniFile)-i] == '\\' )
		{
			szIniFile[(strlen(szIniFile)-i)+1] = '\0';
			strcat(szIniFile, "settings.cfg" );
			return;
		}		
	}
}

#define READ(name, zahl) if(!lstrcmp(Option, name)) zahl = Inhalt;

void SetSettings()
{	
	FILE* Parse;
	Parse = fopen(szIniFile, "r");
	if(!Parse)
		return;

	char Option[255] = "";
	int Inhalt = 0;

	while(!feof(Parse))
	{
		if(fscanf(Parse, "%s %d", &Option, &Inhalt) != 2)
			return;

		READ("Bot", cvars.Aimbot);
		READ("Key", cvars.Aimkey);
		READ("Fov", cvars.Fov);
		READ("Spot", cvars.Spot);
		READ("Draw", cvars.Draw);
		READ("Mode", cvars.Mode);
		READ("Aimtrough", cvars.Aimtrough);

		READ("Name", cvars.Name);
		READ("Box",cvars.Box);
		READ("Distance",cvars.Distance);
		READ("Health",cvars.Health);
		READ("SteamID",cvars.GUID);
		READ("Class",cvars.Class);
		READ("Object",cvars.Object);
		READ("Visible",cvars.Visible);

		READ("Crosshair",cvars.Crosshair);
		READ("Autopistol",cvars.Autopistol);
		READ("Radar", cvars.Radar);
		READ("Bunnyhop", cvars.Bunnyhop);
		READ("Speedhack", cvars.Speedhack);
		READ("Speedkey", cvars.Speedkey);

		READ("Menukey",cvars.Menukey);
		READ("Panickey", cvars.Panickey);
	}
	fclose(Parse);
}


BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	if(fdwReason == 1)
	{
		GetIniFile(hinstDLL);
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)HookD3D, 0, 0, 0);
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)GetAndHookFuncs, 0, 0, 0);
		SetSettings();
		HideModule(hinstDLL);
	}
	else if(fdwReason == 2)
	{
		//
	}
	return true;
}

void HideModule(HINSTANCE hModule)
{
	DWORD dwPEB_LDR_DATA = 0;
	_asm
	{
		pushad;
		pushfd;
		mov eax, fs:[30h]		
		mov eax, [eax+0Ch]
		mov dwPEB_LDR_DATA, eax	 

InLoadOrderModuleList:
		mov esi, [eax+0Ch]
		mov edx, [eax+10h]

		LoopInLoadOrderModuleList: 
		    lodsd
			mov esi, eax
			mov ecx, [eax+18h]
			cmp ecx, hModule
			jne SkipA
		    	mov ebx, [eax]
		    	mov ecx, [eax+4]
		    	mov [ecx], ebx
		    	mov [ebx+4], ecx
			jmp InMemoryOrderModuleList
		SkipA:
			cmp edx, esi
			jne LoopInLoadOrderModuleList

InMemoryOrderModuleList:
		mov eax, dwPEB_LDR_DATA	
		mov esi, [eax+14h]
		mov edx, [eax+18h]

		LoopInMemoryOrderModuleList: 
			lodsd
			mov esi, eax
			mov ecx, [eax+10h]
			cmp ecx, hModule
			jne SkipB
				mov ebx, [eax] 
				mov ecx, [eax+4]
				mov [ecx], ebx
				mov [ebx+4], ecx
				jmp InInitializationOrderModuleList
		SkipB:
			cmp edx, esi
			jne LoopInMemoryOrderModuleList

InInitializationOrderModuleList:
		mov eax, dwPEB_LDR_DATA	
		mov esi, [eax+1Ch]
		mov edx, [eax+20h]

		LoopInInitializationOrderModuleList: 
			lodsd
			mov esi, eax		
			mov ecx, [eax+08h]
			cmp ecx, hModule		
			jne SkipC
				mov ebx, [eax] 
				mov ecx, [eax+4]
				mov [ecx], ebx
				mov [ebx+4], ecx
				jmp Finished
		SkipC:
			cmp edx, esi
			jne LoopInInitializationOrderModuleList

		Finished:
			popfd;
			popad;
	}
}
