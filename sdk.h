#ifndef _SDK_H_
#define _SDK_H_

#define GAME_DLL
#define CLIENT_DLL
#define CSTRIKE_DLL
#define __restrict


#pragma warning(disable: 4541 4081 4018 4089 4715 4313 4102 4005)

#pragma comment(lib, "SDK/lib/mathlib.lib");
//#pragma comment(lib, "SDK/lib/public/bitmap.lib");
//#pragma comment(lib, "SDK/lib/public/choreoobjects.lib");
//#pragma comment(lib, "SDK/lib/public/dmxloader.lib");
//#pragma comment(lib, "SDK/lib/public/nvtristrip.lib");
//#pragma comment(lib, "SDK/lib/public/particles.lib");
//#pragma comment(lib, "SDK/lib/public/raytrace.lib");
//#pragma comment(lib, "SDK/lib/public/steam_api.lib");
#pragma comment(lib, "SDK/lib/tier0.lib");
//#pragma comment(lib, "SDK/lib/public/tier1.lib");
//#pragma comment(lib, "SDK/lib/public/tier2.lib");
//#pragma comment(lib, "SDK/lib/public/tier3.lib");
//#pragma comment(lib, "SDK/lib/public/vgui_controls.lib");
//#pragma comment(lib, "SDK/lib/public/vmpi.lib");
//#pragma comment(lib, "SDK/lib/public/vstdlib.lib");
//#pragma comment(lib, "SDK/lib/public/vtf.lib");

//#include "SDK/public/mathlib/mathlib.h"
#include "SDK/public/cdll_int.h"
#include "SDK/public/client_class.h"
#include "SDK/public/dlight.h"
#include "SDK/public/dt_recv.h"
#include "SDK/public/filesystem.h"
#include "SDK/public/iefx.h"
#include "SDK/public/icvar.h"
#include "SDK/public/igameevents.h"
#include "SDK/public/inetchannelinfo.h"
#include "SDK/public/icliententity.h"
#include "SDK/public/icliententitylist.h"
#include "SDK/public/model_types.h"
#include "SDK/public/pixelwriter.h"
#include "SDK/public/studio.h"
#include "SDK/public/view_shared.h"
#include "SDK/public/ienginevgui.h"

#include "SDK/public/ivrenderview.h"
#include "SDK/cl_dll/iviewrender.h"
#include "SDK/cl_dll/viewrender.h"

#include "SDK/cl_dll/cbase.h"
#include "SDK/cl_dll/input.h"

#include "SDK/public/engine/ivmodelrender.h"
#include "SDK/public/engine/ivmodelinfo.h"
#include "SDK/public/engine/IEngineTrace.h"
#include "SDK/public/engine/ivdebugoverlay.h"

#include "SDK/game_shared/usercmd.h"
#include "SDK/game_shared/in_buttons.h"

#include "SDK/public/materialsystem/imaterialsystem.h"
#include "SDK/public/materialsystem/imaterialsystemstub.h"
#include "SDK/public/materialsystem/imesh.h"
#include "SDK/public/materialsystem/imaterial.h"
#include "SDK/public/materialsystem/imaterialvar.h"
#include "SDK/public/materialsystem/MaterialSystemUtil.h"

#include "SDK/public/vgui/ISurface.h"
//#include "SDK/public/VGuiMatSurface/IMatSystemSurface.h"
//#include "SDK/public/appframework/IAppSystem.h"
//#include "SDK/common/GameUI/IGameConsole.h"

class HL2
{
	public:
	
		IEngineTrace*			pEnginetrace;
		IVEngineClient*			pEngineFuncs;
		IBaseClientDLL*			pClientFuncs;
		IClientEntityList*		pEntityList;
		IVModelInfoClient*		pModelInfo;
		IMaterialSystem*		pMaterialSystem;
		//ICvar*					pCvar;
		IVDebugOverlay*			pDebugOverlay;
		//IGameEventManager2*		pGameEventManager;
		CInput*					pInput;
		//CGlobalVarsBase*		pGlobals;
		//IMatSystemSurface*		pDraw;
		vgui::ISurface*			pSurface;
		IEngineVGui*			pEnginevgui;

}; extern HL2 hl2;

//#define _WIN32_WINNT 0x0501

#include <windows.h>
#include <string>
#include <iostream>
#include <fstream>

#include "DLLMain.h"
#include "Tools.h"
#include "Menu.h"
#include "Hook.h"
#include "D3D.h"
#include "ESP.h"
#include "Aimbot.h"

#include "SDK\\D3D\\d3d9.h"
#include "SDK\\D3D\\d3dx9.h"

using namespace std;

#endif