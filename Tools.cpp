#include "SDK.h"

#define M_PI		3.14159265358979323846

void LogText(char* szText, ...)
{
	char szBuffer[1024];
	va_list args;
    va_start(args, szText);
    memset(szBuffer, 0, sizeof(szBuffer));
    _vsnprintf(szBuffer, sizeof(szBuffer) - 1, szText, args);
    va_end(args);

	std::ofstream LOG("C:\\tf2.txt",std::ios::app);
	LOG << szBuffer << std::endl;
	LOG.close();
}

void *DetourFunc(BYTE *src, const BYTE *dst, const int len)
{
	BYTE *jmp = (BYTE*)malloc(len+5);
	DWORD dwback;

	VirtualProtect(src, len, PAGE_READWRITE, &dwback);

	memcpy(jmp, src, len);	jmp += len;
	
	jmp[0] = 0xE9;
	*(DWORD*)(jmp+1) = (DWORD)(src+len - jmp) - 5;

	src[0] = 0xE9;
	*(DWORD*)(src+1) = (DWORD)(dst - src) - 5;

	VirtualProtect(src, len, dwback, &dwback);

	return (jmp-len);
}

//bool RetourFunc(BYTE *src, BYTE *restore, const int len)
//{
//	DWORD dwback;
//		
//	if(!VirtualProtect(src, len, PAGE_READWRITE, &dwback))	{ return false; }
//	if(!memcpy(src, restore, len))							{ return false; }
//
//	restore[0] = 0xE9;
//	*(DWORD*)(restore+1) = (DWORD)(src - restore) - 5;
//
//	if(!VirtualProtect(src, len, dwback, &dwback))			{ return false; }
//	
//	return true;
//}

bool bDataCompare(const BYTE* pData, const BYTE* bMask, const char* szMask)
{
	for(;*szMask;++szMask,++pData,++bMask)
		if(*szMask=='x' && *pData!=*bMask ) 
			return false;
	return (*szMask) == NULL;
}

DWORD dwFindPattern(DWORD dwAddress,DWORD dwLen,BYTE *bMask,char * szMask)
{
	for(DWORD i=0; i < dwLen; i++)
		if( bDataCompare( (BYTE*)( dwAddress+i ),bMask,szMask) )
			return (DWORD)(dwAddress+i);

	return 0;
}

//bool BackupMemory(DWORD pvAddress, PBYTE bpBackup, int size)
//{
//	if(!ReadProcessMemory(GetCurrentProcess(), (PVOID)pvAddress, bpBackup, size, 0))
//		return false;
//
//	return true;
//}
//
//bool RestoreMemory(DWORD pvAddress, PBYTE bpBackup, int size)
//{
//	if(!WriteProcessMemory(GetCurrentProcess(), (PVOID)pvAddress, bpBackup, size, 0))
//		return false;
//
//	return true;
//}

//void VectorSubtract(float *in, float *sub, float *res)
//{
//	/*for( int i = 0; i < 3; i++ )
//		res[i] = in[i] - sub[i];*/
//	res[0] = in[0] - sub[0];
//	res[1] = in[1] - sub[1];
//	res[2] = in[2] - sub[2];
//}

float VecDistance( Vector from,Vector to ) 
{ 
    Vector angle; 

    angle.x = to.x - from.x; 
    angle.y = to.y - from.y; 
    angle.z = to.z - from.z; 

    return sqrt(angle.x*angle.x + angle.y*angle.y + angle.z*angle.z); 
} 

float GetDistance(CBaseEntity* pLocal, CBaseEntity* pEntity)
{
	Vector vDiff = pEntity->GetAbsOrigin() - pLocal->GetAbsOrigin();

	return sqrt(vDiff.x*vDiff.x + vDiff.y*vDiff.y + vDiff.z*vDiff.z);
}

bool IsInPixelFov (int x, int y, int fov) 
{	
	if(fov == 0)
		return true;
	
	D3DVIEWPORT9 oViewport;
	pD3Ddev->GetViewport(&oViewport);
	
	int centerX = oViewport.Width/2;
	int centerY = oViewport.Height/2;

	if(x >= (centerX-fov) && x <= (centerX+fov) && y >= (centerY-fov) && y <= (centerY+fov))
		return true;
	else
		return false;

	return 0;
}

void DrawFilledQuad(int x, int y, int w, int h, int r, int g, int b, int a)
{
	D3DCOLOR rgba = D3DCOLOR_RGBA(r,g,b,a);

	D3DRECT box = {	x, y, x+w, y+h	};
	pD3Ddev->Clear(1, &box, D3DCLEAR_TARGET, rgba, 0, 0);
}

void DrawQuad(int x, int y, int w, int h, int r, int g, int b, int a, int thickness)
{
	D3DCOLOR rgba = D3DCOLOR_RGBA(r,g,b,a);

	D3DRECT oben =		{ x,	y,		x+w,		   y+thickness		};
	D3DRECT unten =		{ x,	y+h,	x+w,		   y+h+thickness	};
	D3DRECT links =		{ x,	y,		x+thickness,   y+h				};
	D3DRECT rechts =	{ x+w,	y,		x+w+thickness, y+h+thickness	};

	pD3Ddev->Clear(1, &oben, D3DCLEAR_TARGET, rgba, 0, 0);
	pD3Ddev->Clear(1, &links, D3DCLEAR_TARGET, rgba, 0, 0);
	pD3Ddev->Clear(1, &rechts, D3DCLEAR_TARGET, rgba, 0, 0);
	pD3Ddev->Clear(1, &unten, D3DCLEAR_TARGET, rgba, 0, 0);
}

void DrawPixel(int x, int y, int r, int g, int b, int a)
{
	D3DCOLOR rgba = D3DCOLOR_RGBA(r,g,b,a);

	D3DRECT pixel = { x, y, x+1, y+1	};
	pD3Ddev->Clear(1, &pixel, D3DCLEAR_TARGET, rgba, 0, 0);
}

//#define M_RADPI 57.295779513082f
//int bla = 7;
//void CalcAngle( const Vector& vSource, const Vector& vDest, QAngle& vAngles )
//{
//	double delta[3] = { (vSource[0]-vDest[0]), (vSource[1]-vDest[1]), (vSource[2]-vDest[2]) };
//	/*if(GetAsyncKeyState(VK_NUMPAD4) & 1) --bla;
//	if(GetAsyncKeyState(VK_NUMPAD6) & 1) ++bla;*/
//	delta[2] += -5;
//	//drawString2(0, 20, 20, 255, 0, 0, 255, "%d", bla);
//	double hyp = sqrt( delta[0]*delta[0] + delta[1]*delta[1] );
//
//	vAngles[0] = (float) (atan((delta[2])/hyp) * M_RADPI);
//	vAngles[1] = (float) (atan(delta[1]/delta[0]) * M_RADPI);
//	vAngles[2] = 0.0f;
//
//	if( delta[0] >= 0.0f ) vAngles[1] += 180.0f;
//}

void pixel(int x, int y, int w, int h, int r, int g, int b, int a)
{
	DrawFilledQuad(x, y, w, h, r, g, b, a);
}

int iGetLength(const char *pInput, ...)
{
	return strlen(pInput);
}

int iGetWidth(const char *pInput, ...)
{
  char buf[256];
  va_list arguments;

  va_start(arguments, pInput);
  vsprintf(buf, pInput, arguments);
  va_end(arguments);

  int len=0;

  for (int i=0;i<sizeof(buf);i++)
  {
    if (buf[i]=='\0')
    {
      return len;
    }
    else if (buf[i]=='a' || buf[i]=='A' || buf[i]=='b' || buf[i]=='B' || buf[i]=='8' || buf[i]=='c' || buf[i]=='C' || buf[i]=='d' || buf[i]=='D' || buf[i]=='e' || buf[i]=='E' || buf[i]=='f' || buf[i]=='F' || buf[i]=='g' || buf[i]=='G' || buf[i]=='h' || buf[i]=='H' || buf[i]=='j' || buf[i]=='J' || buf[i]=='k' || buf[i]=='K' || buf[i]=='l' || buf[i]=='L' || buf[i]=='n' || buf[i]=='N' || buf[i]=='o' || buf[i]=='O' || buf[i]=='0' || buf[i]=='p' || buf[i]=='P' || buf[i]=='r' || buf[i]=='R' || buf[i]=='s' || buf[i]=='S' || buf[i]=='5' || buf[i]=='u' || buf[i]=='U' || buf[i]=='v' || buf[i]=='V' || buf[i]=='x' || buf[i]=='X' || buf[i]=='y' || buf[i]=='Y' || buf[i]=='z' || buf[i]=='Z' || buf[i]=='2' || buf[i]=='3' || buf[i]=='4' || buf[i]=='6' || buf[i]=='7' || buf[i]=='9' || buf[i]=='-' || buf[i]=='_')
    {
      len+=6;
    }
    else if (buf[i]=='m' || buf[i]=='M' || buf[i]=='q' || buf[i]=='Q' || buf[i]=='w' || buf[i]=='W' || buf[i]=='+')
    {
      len+=7;
    }
    else if (buf[i]=='i' || buf[i]=='I' || buf[i]=='1' || buf[i]=='t' || buf[i]=='T' || buf[i]=='.' || buf[i]==',' || buf[i]==':' || buf[i]=='!' || buf[i]=='/' || buf[i]=='(' || buf[i]==')' || buf[i]=='[' || buf[i]==']')
    {
      len+=5;
    }
    else if (buf[i]==' ')
    {
      len+=4;
    }
  }
  return len;
}

void drawString2(bool bCenter, int x, int y, int r, int g, int b,int a, const char *pInput, ...)
{
  char buf[256];
  va_list arguments;

  va_start(arguments, pInput);
  vsprintf(buf, pInput, arguments);
  va_end(arguments);

  if (bCenter)
  {
    int iLength;
    iLength=iGetWidth(buf);
    x=x-iLength/2;
  }

  int oldX=x;

  int bg_r, bg_g, bg_b;
  if (r==0 && g==0 && b==0)
  {
    bg_r=180;
    bg_g=180;
    bg_b=180;
  }
  else
  {
    bg_r=0;
    bg_g=0;
    bg_b=0;
  }

  //oglSubtractive=true;

  for (int i=0;i<sizeof(buf);i++)
  {
    if (buf[i]=='\0')
    {
      //oglSubtractive=false;
      return;
    }
    else if (buf[i]==' ')
    {
      x+=4;
    }
    if (buf[i]=='\n')
    {
      y+=11;
      x=oldX;
    }
    else if (buf[i]=='a' || buf[i]=='A')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 4, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 4, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='b' || buf[i]=='B' || buf[i]=='8')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 3, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 3, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='c' || buf[i]=='C')
    {
      //background
      pixel(x, y, 6, 3, bg_r, bg_g, bg_b, 255);
      pixel(x, y+3, 3, 1, bg_r, bg_g, bg_b, 255);
      pixel(x, y+4, 6, 3, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 3, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='d' || buf[i]=='D')
    {
      //background
      pixel(x, y, 5, 7, bg_r, bg_g, bg_b, 255);
      pixel(x+5, y+1, 1, 5, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 3, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 3, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 3, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 3, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='e' || buf[i]=='E')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+2, y+3, 3, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 3, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='f' || buf[i]=='F')
    {
      //background
      pixel(x, y, 6, 5, bg_r, bg_g, bg_b, 255);
      pixel(x, y+5, 3, 2, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+2, y+3, 3, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 4, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='g' || buf[i]=='G')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+3, y+3, 1, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 3, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+3, 1, 2, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='h' || buf[i]=='H')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+1, 1, 5, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+1, 1, 5, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='i' || buf[i]=='I' || buf[i]=='1')
    {
      //background
      pixel(x, y, 5, 3, bg_r, bg_g, bg_b, 255);
      pixel(x+1, y+3, 3, 1, bg_r, bg_g, bg_b, 255);
      pixel(x, y+4, 5, 3, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 3, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 3, 1, r, g, b, 255);
      //balken mitte (senkrecht)
      pixel(x+2, y+2, 1, 3, r, g, b, 255);
      x+=5;
    }
    else if (buf[i]=='j' || buf[i]=='J')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+4, 1, 1, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 3, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='k' || buf[i]=='K')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+1, 1, 5, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+3, 1, 3, r, g, b, 255);
      //punkte
      pixel(x+4, y+1, 1, 1, r, g, b, 255);
      pixel(x+3, y+2, 1, 1, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='l' || buf[i]=='L')
    {
      //background
      pixel(x, y, 3, 7, bg_r, bg_g, bg_b, 255);
      pixel(x+3, y+4, 3, 3, bg_r, bg_g, bg_b, 255);
      //balken links
      pixel(x+1, y+1, 1, 4, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='m' || buf[i]=='M')
    {
      //background
      pixel(x, y, 7, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 5, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 4, r, g, b, 255);
      //balken mitte(senkrecht)
      pixel(x+3, y+2, 1, 4, r, g, b, 255);
      //balken rechts
      pixel(x+5, y+2, 1, 4, r, g, b, 255);
      x+=7;
    }
    else if (buf[i]=='n' || buf[i]=='N')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 4, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 4, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='o' || buf[i]=='O' || buf[i]=='0')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 3, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 3, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='p' || buf[i]=='P')
    {
      //background
      pixel(x, y, 3, 7, bg_r, bg_g, bg_b, 255);
      pixel(x+3, y, 3, 5, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 4, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 2, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='q' || buf[i]=='Q')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      pixel(x+6, y+5, 1, 3, bg_r, bg_g, bg_b, 255);
      pixel(x+4, y+7, 3, 1, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 3, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 3, r, g, b, 255);
      //punkte
      pixel(x+3, y+4, 1, 1, r, g, b, 255);
      pixel(x+5, y+6, 1, 1, r, g, b, 255);
      x+=7;
    }
    else if (buf[i]=='r' || buf[i]=='R')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 4, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 1, r, g, b, 255);
      //punkte
      pixel(x+3, y+4, 2, 1, r, g, b, 255);
      pixel(x+4, y+5, 1, 1, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='s' || buf[i]=='S' || buf[i]=='5')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 2, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+3, 1, 2, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='t' || buf[i]=='T')
    {
      //background
      pixel(x, y, 5, 3, bg_r, bg_g, bg_b, 255);
      pixel(x+1, y+3, 3, 4, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 3, 1, r, g, b, 255);
      //balken mitte(senkrecht)
      pixel(x+2, y+2, 1, 4, r, g, b, 255);
      x+=5;
    }
    else if (buf[i]=='u' || buf[i]=='U')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+1, 1, 4, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+1, 1, 4, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='v' || buf[i]=='V')
    {
      //background
      pixel(x, y, 6, 6, bg_r, bg_g, bg_b, 255);
      pixel(x+1, y+6, 4, 1, bg_r, bg_g, bg_b, 255);
      //balken unten
      pixel(x+2, y+5, 2, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+1, 1, 4, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+1, 1, 4, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='w' || buf[i]=='W')
    {
      //background
      pixel(x, y, 7, 7, bg_r, bg_g, bg_b, 255);
      //balken unten
      pixel(x+1, y+5, 5, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+1, 1, 4, r, g, b, 255);
      //balken mitte(senkrecht)
      pixel(x+3, y+1, 1, 4, r, g, b, 255);
      //balken rechts
      pixel(x+5, y+1, 1, 4, r, g, b, 255);
      x+=7;
    }
    else if (buf[i]=='x' || buf[i]=='X')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken links(oben)
      pixel(x+1, y+1, 1, 2, r, g, b, 255);
      //balken rechts(oben)
      pixel(x+4, y+1, 1, 2, r, g, b, 255);
      //balken links(unten)
      pixel(x+1, y+4, 1, 2, r, g, b, 255);
      //balken rechts(unten)
      pixel(x+4, y+4, 1, 2, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='y' || buf[i]=='Y')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+1, 1, 3, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+1, 1, 4, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='z' || buf[i]=='Z' || buf[i]=='2')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+3, 1, 2, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 2, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='3')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+1, y+3, 3, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 3, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='4')
    {
      //background
      pixel(x, y, 6, 5, bg_r, bg_g, bg_b, 255);
      pixel(x+3, y+5, 3, 2, bg_r, bg_g, bg_b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+1, 1, 3, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+1, 1, 5, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='6')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 3, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+3, 1, 2, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='7')
    {
      //background
      pixel(x, y, 6, 3, bg_r, bg_g, bg_b, 255);
      pixel(x+1, y+3, 5, 2, bg_r, bg_g, bg_b, 255);
      pixel(x+3, y+5, 3, 2, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 4, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='9')
    {
      //background
      pixel(x, y, 6, 7, bg_r, bg_g, bg_b, 255);
      //balken oben
      pixel(x+1, y+1, 4, 1, r, g, b, 255);
      //balken mitte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      //balken unten
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      //balken links
      pixel(x+1, y+2, 1, 2, r, g, b, 255);
      //balken rechts
      pixel(x+4, y+2, 1, 3, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='.')
    {
      //background
      pixel(x+1, y+4, 3, 3, bg_r, bg_g, bg_b, 255);
      //punkt
      pixel(x+2, y+5, 1, 1, r, g, b, 255);
      x+=5;
    }
    else if (buf[i]==',')
    {
      //background
      pixel(x+1, y+4, 3, 4, bg_r, bg_g, bg_b, 255);
      //punkt
      pixel(x+2, y+5, 1, 2, r, g, b, 255);
      x+=5;
    }
    else if (buf[i]==':')
    {
      //background
      pixel(x+1, y+1, 3, 6, bg_r, bg_g, bg_b, 255);
      //punkte
      pixel(x+2, y+2, 1, 1, r, g, b, 255);
      pixel(x+2, y+5, 1, 1, r, g, b, 255);
      x+=5;
    }
    else if (buf[i]=='!')
    {
      //background
      pixel(x+1, y, 3, 7, bg_r, bg_g, bg_b, 255);
      //punkte
      pixel(x+2, y+1, 1, 3, r, g, b, 255);
      pixel(x+2, y+5, 1, 1, r, g, b, 255);
      x+=5;
    }
    else if (buf[i]=='/')
    {
      //background
      pixel(x, y, 3, 2, bg_r, bg_g, bg_b, 255);
      pixel(x, y+2, 4, 2, bg_r, bg_g, bg_b, 255);
      pixel(x+1, y+4, 4, 2, bg_r, bg_g, bg_b, 255);
      pixel(x+2, y+6, 3, 2, bg_r, bg_g, bg_b, 255);
      //punkte
      pixel(x+1, y+1, 1, 2, r, g, b, 255);
      pixel(x+2, y+3, 1, 2, r, g, b, 255);
      pixel(x+3, y+5, 1, 2, r, g, b, 255);
      x+=5;
    }
    else if (buf[i]=='(')
    {
      //background
      pixel(x+1, y, 4, 1, bg_r, bg_g, bg_b, 255);
      pixel(x, y+1, 5, 2, bg_r, bg_g, bg_b, 255);
      pixel(x, y+3, 3, 2, bg_r, bg_g, bg_b, 255);
      pixel(x, y+5, 5, 2, bg_r, bg_g, bg_b, 255);
      pixel(x+1, y+7, 4, 1, bg_r, bg_g, bg_b, 255);
      //punkte
      pixel(x+2, y+1, 2, 1, r, g, b, 255);
      pixel(x+1, y+2, 1, 4, r, g, b, 255);
      pixel(x+2, y+6, 2, 1, r, g, b, 255);
      x+=5;
    }
    else if (buf[i]==')')
    {
      //background
      pixel(x, y, 4, 1, bg_r, bg_g, bg_b, 255);
      pixel(x, y+1, 5, 2, bg_r, bg_g, bg_b, 255);
      pixel(x+2, y+3, 3, 2, bg_r, bg_g, bg_b, 255);
      pixel(x, y+5, 5, 2, bg_r, bg_g, bg_b, 255);
      pixel(x, y+7, 4, 1, bg_r, bg_g, bg_b, 255);
      //punkte
      pixel(x+1, y+1, 2, 1, r, g, b, 255);
      pixel(x+3, y+2, 1, 4, r, g, b, 255);
      pixel(x+1, y+6, 2, 1, r, g, b, 255);
      x+=5;
    }
    else if (buf[i]=='[')
    {
      //background
      pixel(x, y, 5, 3, bg_r, bg_g, bg_b, 255);
      pixel(x, y+3, 3, 2, bg_r, bg_g, bg_b, 255);
      pixel(x, y+5, 5, 3, bg_r, bg_g, bg_b, 255);
      //punkte
      pixel(x+1, y+1, 3, 1, r, g, b, 255);
      pixel(x+1, y+2, 1, 4, r, g, b, 255);
      pixel(x+1, y+6, 3, 1, r, g, b, 255);
      x+=5;
    }
    else if (buf[i]==']')
    {
      //background
      pixel(x, y, 5, 3, bg_r, bg_g, bg_b, 255);
      pixel(x+2, y+3, 3, 2, bg_r, bg_g, bg_b, 255);
      pixel(x, y+5, 5, 3, bg_r, bg_g, bg_b, 255);
      //punkte
      pixel(x+1, y+1, 3, 1, r, g, b, 255);
      pixel(x+3, y+2, 1, 4, r, g, b, 255);
      pixel(x+1, y+6, 3, 1, r, g, b, 255);
      x+=5;
    }
    else if (buf[i]=='-')
    {
      //background
      pixel(x+1, y+2, 4, 3, bg_r, bg_g, bg_b, 255);
      //punkte
      pixel(x+2, y+3, 2, 1, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='_')
    {
      //background
      pixel(x, y+4, 6, 3, bg_r, bg_g, bg_b, 255);
      //punkte
      pixel(x+1, y+5, 4, 1, r, g, b, 255);
      x+=6;
    }
    else if (buf[i]=='+')
    {
      //background
      pixel(x+2, y, 3, 2, bg_r, bg_g, bg_b, 255);
      pixel(x, y+2, 7, 3, bg_r, bg_g, bg_b, 255);
      pixel(x+2, y+5, 3, 2, bg_r, bg_g, bg_b, 255);
      //punkte
      pixel(x+3, y+1, 1, 2, r, g, b, 255);
      pixel(x+1, y+3, 5, 1, r, g, b, 255);
      pixel(x+3, y+4, 1, 2, r, g, b, 255);
      x+=7;
    }
  }
}

void Crosshair(int type)
{
	D3DVIEWPORT9 oViewport;
	pD3Ddev->GetViewport(&oViewport);
	
	int centerX = oViewport.Width/2;
	int centerY = oViewport.Height/2;
	
	if(type == 1)
	{
		DrawFilledQuad(centerX - 14, centerY, 9, 1,255,255,255,255);
		DrawFilledQuad(centerX +5, centerY, 9, 1,255,255,255,255);
		DrawFilledQuad(centerX, centerY - 14, 1, 9,255,255,255,255);
		DrawFilledQuad(centerX, centerY + 5, 1, 9,255,255,255,255);
		DrawFilledQuad(centerX, centerY , 1, 1,255,0,0,255);
	}
	else if(type == 2)
	{
		DrawFilledQuad(centerX - 14, centerY, 9, 2,255,255,255,255);
		DrawFilledQuad(centerX +5, centerY, 9, 2,255,255,255,255);
		DrawFilledQuad(centerX, centerY - 14, 2, 9,255,255,255,255);
		DrawFilledQuad(centerX, centerY + 5, 2, 9,255,255,255,255);
		DrawFilledQuad(centerX, centerY , 1, 2,255,0,0,255);
	}
	else if(type == 3)
	{
		DrawFilledQuad(centerX-25,centerY,50,1,255,255,255,255);
		DrawFilledQuad(centerX-5,centerY,10,1,255,0,0,255);
		DrawFilledQuad(centerX,centerY-25,1,50,255,255,255,255);
		DrawFilledQuad(centerX,centerY-5,1,10,255,0,0,255);
	}
	else if(type == 4)
	{
		DrawFilledQuad(centerX-25,centerY,50,2,255,255,255,255);
		DrawFilledQuad(centerX-5,centerY,10,2,255,0,0,255);
		DrawFilledQuad(centerX,centerY-25,2,50,255,255,255,255);
		DrawFilledQuad(centerX,centerY-5,2,10,255,0,0,255);
	}
	else if(type == 5)
	{
		DrawQuad(centerX,0,0,centerY*2,255,255,255,255,1);
		DrawQuad(0,centerY,centerX*2,0,255,255,255,255,1);
		DrawFilledQuad(centerX,centerY-5,1,10,255,0,0,255);
		DrawFilledQuad(centerX-5,centerY,10,1,255,0,0,255);	
	}
}

bool ScreenTransform( const Vector &point, Vector &screen )
{
	float w;
	const VMatrix &worldToScreen = hl2.pEngineFuncs->WorldToScreenMatrix();
	screen.x = worldToScreen[0][0] * point[0] + worldToScreen[0][1] * point[1] + worldToScreen[0][2] * point[2] + worldToScreen[0][3];
	screen.y = worldToScreen[1][0] * point[0] + worldToScreen[1][1] * point[1] + worldToScreen[1][2] * point[2] + worldToScreen[1][3];
	w		 = worldToScreen[3][0] * point[0] + worldToScreen[3][1] * point[1] + worldToScreen[3][2] * point[2] + worldToScreen[3][3];
	screen.z = 0.0f;

	bool behind = false;

	if( w < 0.001f )
	{
		behind = true;
		screen.x *= 100000;
		screen.y *= 100000;
	}
	else
	{
		behind = false;
		float invw = 1.0f / w;
		screen.x *= invw;
		screen.y *= invw;
	}
	return behind;
}

bool WorldToScreen( const Vector &vOrigin, Vector &vScreen )
{
	if( ScreenTransform(vOrigin , vScreen) == false )
	{
		int iScreenWidth, iScreenHeight;
		
		hl2.pEngineFuncs->GetScreenSize( iScreenWidth, iScreenHeight );
		float x = iScreenWidth / 2;
		float y = iScreenHeight / 2;
		x += 0.5 * vScreen.x * iScreenWidth + 0.5;
		y -= 0.5 * vScreen.y * iScreenHeight + 0.5;
		vScreen.x = x;
		vScreen.y = y;
		return true;
	}
	return false;
}

//void GetWorldSpaceCenter( CBaseEntity* pBaseEnt, Vector& vWorldSpaceCenter )
//{
//	if ( pBaseEnt )
//	{
//		Vector vMin, vMax;
//		pBaseEnt->GetRenderBounds( vMin, vMax );
//		vWorldSpaceCenter = pBaseEnt->GetAbsOrigin();
//		vWorldSpaceCenter.z += (vMin.z + vMax.z) / 2.0f;
//	}
//}
bool bGetVisible(int i, const Vector& vecAbsStart, const Vector& vecAbsEnd) 
{
    player_info_t pinfo;
	trace_t tr;
	Ray_t ray;

	ray.Init( vecAbsStart, vecAbsEnd );
	hl2.pEnginetrace->TraceRay( ray, MASK_NPCWORLDSTATIC, NULL, &tr );

	if ( tr.fraction > 0.97f )
		return true;

	if ( tr.m_pEnt )
	{
		if ( tr.m_pEnt->index == 0 || tr.allsolid )
			return false;

		if (( hl2.pEngineFuncs->GetPlayerInfo( tr.m_pEnt->index, &pinfo ) || i == tr.m_pEnt->index) && tr.fraction > 0.92)
			return true;
	}
	return false;
}



CBaseEntity* pGetBaseEnt( int iIndex )
{
	IClientEntity* pClientEntity = hl2.pEntityList->GetClientEntity( iIndex );

	if ( pClientEntity == NULL )
		return NULL;

	if ( pClientEntity->IsDormant() )
		return NULL;

	return pClientEntity->GetBaseEntity();
}

/////////////////////////////////////////////////////////////////////////////////////////////////

CBaseEntity* pGetBaseEnt( IClientEntity* pClientEnt )
{
	if ( pClientEnt == NULL )
		return NULL;

	return pClientEnt->GetBaseEntity();
}
/*10E60226   .  FFD0                      CALL    EAX
10E60228   .  85C0                      TEST    EAX,EAX
10E6022A   .^ 74 AF                     JE      SHORT client.10E601DB
10E6022C   .  E8 CF5BFBFF               CALL    client.10E15E00
10E60231   .  85C0                      TEST    EAX,EAX
10E60233   .  74 16                     JE      SHORT client.10E6024B
10E60235   .  8B10                      MOV     EDX,DWORD PTR DS:[EAX]
10E60237   .  55                        PUSH    EBP
10E60238   .  56                        PUSH    ESI

10E15E00  /$  E8 CBE10000               CALL    client.10E23FD0
10E15E05  |.  85C0                      TEST    EAX,EAX
10E15E07  |.  75 01                     JNZ     SHORT client.10E15E0A
10E15E09  |.  C3                        RETN
10E15E0A  |>  8B10                      MOV     EDX,DWORD PTR DS:[EAX]
10E15E0C  |.  8BC8                      MOV     ECX,EAX
10E15E0E  |.  8B82 F8020000             MOV     EAX,DWORD PTR DS:[EDX+2F8]
10E15E14  \.  FFE0                      JMP     EAX


*/
bool GetBonePosition ( int iBone, Vector& vecOrigin, QAngle qAngles, int index )
{
	if( iBone < 0 || iBone >= 20 )
		return false;

	matrix3x4_t pmatrix[MAXSTUDIOBONES];

	C_BaseEntity* pBaseEnt = pGetBaseEnt( index );
	

	if(pBaseEnt->SetupBones( pmatrix, 128, BONE_USED_BY_HITBOX, 1.0f ) == false ) //gEngine.Time() );
		return false;

	MatrixAngles( pmatrix[ iBone ], qAngles, vecOrigin );

	return true;
}

//$+34DB0  >/$  E8 4BE10000               CALL    client.11093F00
float vis = 0.8;
bool bbGetVisible( const Vector& vecAbsStart, const Vector& vecAbsEnd) 
{
    player_info_t pinfo;
	trace_t tr;
	Ray_t ray;

	ray.Init( vecAbsStart, vecAbsEnd );
	hl2.pEnginetrace->TraceRay( ray, /*MASK_NPCWORLDSTATIC|CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_MONSTER|CONTENTS_WINDOW|CONTENTS_DEBRIS|CONTENTS_HITBOX*/MASK_ALL, NULL, &tr );

	//if(GetAsyncKeyState(VK_NUMPAD4) & 1)
	//	vis -= 0.01;

	//if(GetAsyncKeyState(VK_NUMPAD6) & 1)
	//	vis += 0.01;
	if(tr.allsolid)
		return false;

	//drawString2(0, 20,20,255,0,0,255,"%.2f, %.2f", vis, tr.fractionleftsolid);
	if(tr.fraction >= 0.94f)
		return true;

	return false;
}

bool GetVisible(int i, const Vector& vecAbsStart, const Vector& vecAbsEnd)
{
	if(bbGetVisible(vecAbsStart, vecAbsEnd))
		return true;
	
	if(bGetVisible(i, vecAbsStart, vecAbsEnd))
		return true;	
}

extern DWORD dwGetActiveWeapon;
C_BaseCombatWeapon* GetActiveWeapon()
{
	__asm call dwGetActiveWeapon
}
/*
Scout:
Knife: models/weapons/v_models/v_bat_scout.mdl
Pistol: models/weapons/v_models/v_pistol_scout.mdl
Gun: models/weapons/v_models/v_scattergun_scout.mdl

Soldier:
Knife: models/weapons/v_models/v_shovel_soldier.mdl
Shotgun: models/weapons/v_models/v_shotgun_soldier.mdl
Gun: models/weapons/v_models/v_rocketlauncher_soldier.mdl

Pyro:
Knife: models/weapons/v_models/v_fireaxe_pyro.mdl
Shotgun: models/weapons/v_models/v_shotgun_pyro.mdl
Gun: models/weapons/v_models/v_flamethrower_pyro.mdl

Demoman:

Knife: models/weapons/v_models/v_bottle_demoman.mdl
Gun: models/weapons/v_models/v_stickybomb_launcher_demo.mdl
Gun: models/weapons/v_models/v_grenadelauncher_demo.mdl

Heavy:

Knife: models/weapons/v_models/v_fist_heavy.mdl
Shotgun: models/weapons/v_models/v_shotgun_heavy.mdl
Gun: models/weapons/v_models/v_minigun_heavy.mdl

Engineer:

Knife: models/weapons/v_models/v_pda_engineer.mdl
Knife: models/weapons/v_models/v_builder_engineer.mdl
Knife: models/weapons/v_models/v_wrench_engineer.mdl
Pistol: models/weapons/v_models/v_pistol_engineer.mdl
Shotgun: models/weapons/v_models/v_shotgun_engineer.mdl

Medic:

Knife: models/weapons/v_models/v_bonesaw_medic.mdl
Pistol: models/weapons/v_models/v_syringegun_medic.mdl
Gun: models/weapons/v_models/v_medigun_medic.mdl

Sniper:

Pistol: models/weapons/v_models/v_smg_sniper.mdl
Gun: models/weapons/v_models/v_sniperrifle_sniper.mdl

Spy:

Knife: models/weapons/v_models/v_pda_spy.mdl
Knife: models/weapons/v_models/v_knife_spy.mdl
Knife: models/weapons/v_models/v_sapper_spy.mdl
Pistol: models/weapons/v_models/v_revolver_spy.mdl

*/