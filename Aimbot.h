#ifndef _AIMBOT_H_
#define _AIMBOT_H_

class CAimbot
{
	public:
		void Init();
		void Do(int i, CBaseEntity* pLocal, CBaseEntity* pEntity, player_info_t &pInfo);
		void DO(CBaseEntity* pLocal);
		void GetNearest(int i, CBaseEntity* pLocal, CBaseEntity* pEntity, player_info_t &pInfo);
		int GetHitbox(const char* szModelname);

		int iNearestEnt;
	private:
		QAngle qPlayer;
		Vector vPlayer[65];
		Vector vPosition;
		
		int iHitbox;
		float fNearestDist;
		float fBuf;
}; extern CAimbot gAimbot;

#endif