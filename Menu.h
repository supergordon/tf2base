#ifndef _MENU_
#define _MENU_
//
class cMenu
{
	public:
		cMenu();
		void Draw();
		void DrawEntry(char* Text, int Cvar,int r, int g, int b, int w);
		void Control();
		void Ueberschriften(int i, bool toggle);
		void Aimbots(int i, bool toggle);
		void Esp(int i, bool toggle);
		void Visuals(int i, bool toggle);
		//void Others(int i, bool toggle);

	private:
		bool Aimbot,ESP,Visual,Other;
		int iSelect,iPress;
		int xx,yy;
		int Abstand,Elemente;

}; extern cMenu gMenu;

#endif