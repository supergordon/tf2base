#ifndef _ESP_H_
#define _ESP_H_

#include "SDK.h"

class CESP
{
	public:
		CESP();
		void Do(int i, CBaseEntity* pLocal, CBaseEntity* pEntity, player_info_t &pInfo);
		void DoObject(CBaseEntity* pLocal, CBaseEntity* pEntity);
		inline void SetColors(int iTeamNum);
		inline void SetObjectColors(int iTeamNum);
		inline int Get(int iIndex);
		inline int Gets(int iIndex);
		void DrawRadarPoint(Vector vecOriginx, Vector vecOriginy, QAngle vAngle, int iTeam);

		Vector vEyeHeight;
	private:
		QAngle qPlayer;
		Vector vPlayer;
		Vector vScreen;
		Vector vObject;
		int iEspHeight;
		int iHitbox;
		int rgba[4];
		int rgbas[4];

}; extern CESP gEsp;


#endif